#!/bin/sh

xhost local:root

pactl list sinks short | cut -f1 | xargs -I{} pactl set-sink-mute {} 1
pactl list sinks short | cut -f1 | xargs -I{} pactl set-sink-volume {} 0%
pactl list sources short | grep -v Monitor | cut -f1 | xargs -I{} pactl set-source-mute {} 1
pactl list sources short | grep -v Monitor | cut -f1 | xargs -I{} pactl set-source-volume {} 0%

if [ ! -f "$HOME/.config/sway/initialized" ]; then
    touch -m "$HOME/.config/sway/initialized"
    if which firefox-esr 2>&1 >/dev/null; then
        xdg-settings set default-web-browser firefox-esr.desktop
    fi
    swaycfg color-scheme dark
    swaymsg reload
fi
if [ ! -f "${XDG_STATE_HOME:-$HOME/.local/state}/weasley/dotstow" ]; then
    kitty -e weasley startup
fi
swaymsg workspace "8"
swaymsg layout tabbed
kitty &
firefox &
flatpak run org.ferdium.Ferdium &
flatpak run org.signal.Signal &
thunderbird &
endeavour &
sleep 5
swayrst load default
nextcloud &
